﻿using NUnit.Framework;

namespace BookListTask.Tests
{
    [TestFixture]
    public class BinaryBookListStorageTest
    {
        private BinaryBookListStorage storage;
        private BookListService service;

        [SetUp]
        public void Init()
        {
            this.service = new BookListService();
            this.storage = new BinaryBookListStorage("data.bin");

            Book book1 = new Book("Author1", "Title1", "Publisher", "1-84356-028-3");
            Book book2 = new Book("Author2", "Title2", "Publisher", "0-684-84328-5");
            Book book3 = new Book("Author3", "Title3", "Publisher", "0-8044-2957-X");
            Book book4 = new Book("Author4", "Title4", "Publisher", "0-85131-041-9");
            Book book5 = new Book("Author5", "Title5", "Publisher", "93-86954-21-4");

            service.Add(book1);
            service.Add(book2);
            service.Add(book3);
            service.Add(book4);
            service.Add(book5);
        }

        [Test]
        public void SaveToBinary_Test()
        {
            this.storage.SaveBooks(this.service.GetBooks());

            var actual = this.storage.GetAllBooks();

            CollectionAssert.AreEqual(this.service.GetBooks(), actual);
        }
    }
}
