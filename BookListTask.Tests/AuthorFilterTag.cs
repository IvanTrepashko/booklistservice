﻿using System;

namespace BookListTask.Tests
{
    public class AuthorFilterTag : IFilterTag<string>
    {
        public bool MatchesTag(Book book, string value)
        {
            return book?.Author.Equals(value, StringComparison.OrdinalIgnoreCase) ?? false;
        }
    }
}