﻿using System;
using System.Collections.Generic;

namespace BookListTask.Tests
{
    /// <summary>
    /// Represents <see cref="Book"/> comparer by author.
    /// </summary>
    public class AuthorBookComparer : Comparer<Book>
    {
        /// <summary>
        /// Compares two instances of <see cref="Book"/> class by author.
        /// </summary>
        /// <param name="x">First book.</param>
        /// <param name="y">Second book.</param>
        /// <returns>Result of comparison.</returns>
        public override int Compare(Book x, Book y)
        {
            string xAuthor = x is null ? string.Empty : x.Author;
            string yAuthor = y is null ? string.Empty : y.Author;

            return string.Compare(xAuthor, yAuthor, StringComparison.Ordinal);
        }
    }
}