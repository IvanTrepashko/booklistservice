﻿using System;
using System.Collections.Generic;

namespace BookListTask.Tests
{
    public class FakeBookListStorage : IBookListStorage
    {
        private List<Book> bookList;

        public FakeBookListStorage()
        {
            this.bookList = new List<Book>();
        }

        public FakeBookListStorage(IEnumerable<Book> books)
        {
            this.bookList = new List<Book>(books);
        }

        public IEnumerable<Book> GetAllBooks()
        {
            return this.bookList;
        }

        public void SaveBooks(IEnumerable<Book> books)
        {
            if (books is null)
            {
                throw new ArgumentNullException(nameof(books));
            }

            this.bookList.AddRange(books);
        }
    }
}