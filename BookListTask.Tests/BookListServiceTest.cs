using NUnit.Framework;
using System;
using System.Linq;

namespace BookListTask.Tests
{
    [TestFixture]
    public class BookListServiceTest
    {
        private BookListService service;

        [SetUp]
        public void Init()
        {
            service = new BookListService();

            Book book1 = new Book("Author1", "Title1", "Publisher");
            Book book2 = new Book("Author2", "Title2", "Publisher");
            Book book3 = new Book("Author3", "Title3", "Publisher");
            Book book4 = new Book("Author4", "Title4", "Publisher");
            Book book5 = new Book("Author5", "Title5", "Publisher");

            service.Add(book1);
            service.Add(book2);
            service.Add(book3);
            service.Add(book4);
            service.Add(book5);
        }

        [Test]
        public void BookListService_AddBook()
        {
            Book book = new Book("Author6", "Title6", "Publisher");

            service.Add(book);

            var actual = service.FindByTag(new AuthorFilterTag(), "Author6");

            foreach (var item in actual)
            {
                Assert.AreEqual(book, item);
            }
        }

        [Test]
        public void BookListService_RemoveBook()
        {
            Book book = new Book("Author6", "Title6", "Publisher");

            service.Add(book);
            service.Remove(book);

            Assert.AreEqual(Array.Empty<Book>(), service.FindByTag(new AuthorFilterTag(), "Author6").ToArray());
        }

        [Test]
        public void BookListService_AddAlreadyExistingBook_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                Book book = new Book("Author1", "Title1", "Publisher");
                service.Add(book);
            });
        }

        [Test]
        public void BookListService_RemoveNotExistingBook_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                Book book = new Book("Author6", "Title10", "Publisher");
                service.Remove(book);
            });
        }

        [Test]
        public void BookListService_Contains()
        {
            Book book = new Book("Author6", "Title10", "Publisher");
            service.Add(book);

            Assert.True(service.Contains(book));
        }

        [Test]
        public void BookListService_SortByAuthor()
        {
            BookListService bookListService = new BookListService();

            Book book1 = new Book("Author3", "Title1", "Publisher");
            Book book2 = new Book("Author4", "Title2", "Publisher");
            Book book3 = new Book("Author1", "Title3", "Publisher");
            Book book4 = new Book("Author2", "Title4", "Publisher");
            Book book5 = new Book("Author5", "Title5", "Publisher");

            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);
            bookListService.Add(book4);
            bookListService.Add(book5);

            Book[] books = { book3, book4, book1, book2, book5 };

            bookListService.SortBy(new AuthorBookComparer());

            Assert.AreEqual(books, bookListService.GetBooks());
        }

        [Test]
        public void BookListService_CreateFromIEnumerable()
        {
            Book book1 = new Book("Author3", "Title1", "Publisher");
            Book book2 = new Book("Author4", "Title2", "Publisher");
            Book book3 = new Book("Author1", "Title3", "Publisher");

            Book[] books = { book1, book2, book3 };

            BookListService bookListService = new BookListService(books);

            Assert.AreEqual(books, bookListService.GetBooks());
        }

        [Test]
        public void BookListService_Save()
        {
            Book book1 = new Book("Author3", "Title1", "Publisher");
            Book book2 = new Book("Author4", "Title2", "Publisher");
            Book book3 = new Book("Author1", "Title3", "Publisher");

            Book[] books = { book1, book2, book3 };

            FakeBookListStorage storage = new FakeBookListStorage(books);

            BookListService bookListService = new BookListService();
            bookListService.Load(storage);

            Assert.AreEqual(storage.GetAllBooks(), bookListService.GetBooks());
        }

        [Test]
        public void BookListService_Load()
        {
            Book book1 = new Book("Author3", "Title1", "Publisher");
            Book book2 = new Book("Author4", "Title2", "Publisher");
            Book book3 = new Book("Author1", "Title3", "Publisher");

            Book[] books = { book1, book2, book3 };

            BookListService bookListService = new BookListService(books);

            FakeBookListStorage storage = new FakeBookListStorage();
            bookListService.Save(storage);

            Assert.AreEqual(storage.GetAllBooks(), books);
        }
    }
}
