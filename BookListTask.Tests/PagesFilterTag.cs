﻿namespace BookListTask.Tests
{
    public class PagesFilterTag : IFilterTag<int>
    {
        public bool MatchesTag(Book book, int value)
        {
            return book?.Pages.Equals(value) ?? false;
        }
    }
}