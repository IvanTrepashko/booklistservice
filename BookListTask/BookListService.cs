﻿using System;
using System.Collections.Generic;

namespace BookListTask
{
    /// <summary>
    /// Represents class for working with a sequence of <see cref="Book"/> class.
    /// </summary>
    public class BookListService
    {
        private readonly List<Book> books;

        /// <summary>
        /// Initializes new instance of <see cref="BookListService"/> class.
        /// </summary>
        public BookListService()
        {
            this.books = new List<Book>();
        }

        /// <summary>
        /// Initializes new instance of <see cref="BookListService"/> class from IEnumerable source.
        /// </summary>
        /// <param name="source">Source sequence.</param>
        /// <exception cref="ArgumentNullException">Thrown when source is null.</exception>
        public BookListService(IEnumerable<Book> source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            this.books = new List<Book>(source);
        }

        /// <summary>
        /// Adds new book.
        /// </summary>
        /// <param name="item">New book to add.</param>
        /// <exception cref="ArgumentNullException">Thrown when item is null.</exception>
        /// <exception cref="ArgumentException">Thrown when this book already exists.</exception>
        public void Add(Book item)
        {
            if (item is null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            if (this.books.Contains(item))
            {
                throw new ArgumentException("This book already exists.");
            }

            this.books.Add(item);
        }

        /// <summary>
        /// Clears all books.
        /// </summary>
        public void Clear()
        {
            this.books.Clear();
        }

        /// <summary>
        /// Determines if given book exists.
        /// </summary>
        /// <param name="item">Book.</param>
        /// <returns>True if book exists, else return false.</returns>
        /// <exception cref="ArgumentNullException">Thrown when item is null.</exception>
        public bool Contains(Book item)
        {
            if (item is null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            return this.books.Contains(item);
        }

        /// <summary>
        /// Removes book from collection.
        /// </summary>
        /// <param name="item">Book to remove.</param>
        /// <returns>True if book was removed.</returns>
        /// <exception cref="ArgumentNullException">Thrown when item is null.</exception>
        /// <exception cref="ArgumentException">Thrown when this book doesn't exist.</exception>
        public void Remove(Book item)
        {
            if (item is null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            if (!this.Contains(item))
            {
                throw new ArgumentException("This book does not exist.");
            }

            this.books.Remove(item);
        }

        /// <summary>
        /// Finds all books that match specified tag.
        /// </summary>
        /// <param name="filterTag">Filter tag.</param>
        /// <param name="value">Value to look for.</param>
        /// <typeparam name="T">Type of value.</typeparam>
        /// <returns>All books that match specified tag.</returns>
        /// <exception cref="ArgumentNullException">Thrown when filterTag is null.</exception>
        public IEnumerable<Book> FindByTag<T>(IFilterTag<T> filterTag, T value)
        {
            if (filterTag is null)
            {
                throw new ArgumentNullException(nameof(filterTag));
            }

            foreach (var t in this.books)
            {
                if (filterTag.MatchesTag(t, value))
                {
                    yield return t;
                }
            }
        }

        /// <summary>
        /// Gets all books.
        /// </summary>
        /// <returns>Books.</returns>
        public IEnumerable<Book> GetBooks() => this.books;

        /// <summary>
        /// Sorts books by specified comparer.
        /// </summary>
        /// <param name="comparer">Comparer.</param>
        /// <exception cref="ArgumentNullException">Thrown when comparer is null.</exception>
        public void SortBy(IComparer<Book> comparer)
        {
            if (comparer is null)
            {
                throw new ArgumentNullException(nameof(comparer));
            }

            for (int i = 1; i < this.books.Count; i++)
            {
                Book temp = this.books[i];

                for (int j = i - 1; j >= 0; j--)
                {
                    if (comparer.Compare(this.books[j], temp) < 0)
                    {
                        break;
                    }

                    if (comparer.Compare(this.books[j + 1], this.books[j]) < 0)
                    {
                        Book tmp = books[j + 1];
                        books[j + 1] = books[j];
                        books[j] = tmp;
                    }
                }
            }
        }

        /// <summary>
        /// Saves current book list to specified storage.
        /// </summary>
        /// <param name="storage">Storage to save to.</param>
        /// <exception cref="ArgumentNullException">Thrown when storage is null.</exception>
        public void Save(IBookListStorage storage)
        {
            if (storage is null)
            {
                throw new ArgumentNullException(nameof(storage));
            }

            storage.SaveBooks(this.books);
        }

        /// <summary>
        /// Loads all books from specified storage.
        /// </summary>
        /// <param name="storage">Storage to load from.</param>
        /// <exception cref="ArgumentNullException">Thrown when storage is null.</exception>
        public void Load(IBookListStorage storage)
        {
            if (storage is null)
            {
                throw new ArgumentNullException(nameof(storage));
            }

            foreach (var book in storage.GetAllBooks())
            {
                this.Add(book);
            }
        }
    }
}
