﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace BookListTask
{
    public class BinaryBookListStorage : IBookListStorage
    {
        private readonly string path;

        public BinaryBookListStorage(string path)
        {
            this.path = path;
        }

        public IEnumerable<Book> GetAllBooks()
        {
            using FileStream fs = File.OpenRead(this.path);
            using BinaryReader reader = new BinaryReader(fs);

            List<Book> books = new List<Book>();

            while (reader.PeekChar() != -1)
            {
                string author = reader.ReadString();
                int pages = reader.ReadInt32();
                string publisher = reader.ReadString();
                string title = reader.ReadString();
                double price = reader.ReadDouble();
                string isbn = reader.ReadString();
                DateTime date = new DateTime(reader.ReadInt64());

                Book book = new Book(author, title, publisher, isbn)
                {
                    Pages = pages
                };

                book.SetPrice(price, RegionInfo.CurrentRegion);
                book.Publish(date);

                books.Add(book);
            }

            return books;
        }

        public void SaveBooks(IEnumerable<Book> books)
        {
            using FileStream fs = File.Create(path);
            using BinaryWriter writer = new BinaryWriter(fs);

            foreach (var book in books)
            {
                writer.Write(book?.Author);
                writer.Write(book.Pages);
                writer.Write(book?.Publisher);
                writer.Write(book?.Title);
                writer.Write(book.Price);
                writer.Write(book.ISBN ?? string.Empty);
                writer.Write(book.PublicationDate.ToBinary());
            }
        }
    }
}
