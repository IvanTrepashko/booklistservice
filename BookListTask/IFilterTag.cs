﻿namespace BookListTask
{
    /// <summary>
    /// Provides method for filtering books.
    /// </summary>
    /// <typeparam name="T">Type of filter value.</typeparam>
    public interface IFilterTag<in T>
    {
        /// <summary>
        /// Determines if book matches filter tag.
        /// </summary>
        /// <param name="book">Book.</param>
        /// <param name="value">Value to match.</param>
        /// <returns>Result of comparison.</returns>
        bool MatchesTag(Book book, T value);
    }
}