﻿using System.Collections.Generic;

namespace BookListTask
{
    /// <summary>
    /// Provides methods for <see cref="Book"/> storage.
    /// </summary>
    public interface IBookListStorage
    {
        /// <summary>
        /// Loads all books from the storage.
        /// </summary>
        /// <returns>Sequence of books.</returns>
        public IEnumerable<Book> GetAllBooks();

        /// <summary>
        /// Saves books in the storage.
        /// </summary>
        /// <param name="books">Books to save.</param>
        public void SaveBooks(IEnumerable<Book> books);
    }
}