﻿using System;
using System.Globalization;

namespace BookListTask
{
    /// <summary>
    /// Represents book.
    /// </summary>
    public class Book : IEquatable<Book>, IComparable, IComparable<Book>
    {
        private bool isPublished;
        private DateTime datePublished;
        private int totalPages;

        public static bool operator <(Book lhs, Book rhs)
        {
            return lhs.CompareTo(rhs) < 0;
        }

        public static bool operator >(Book lhs, Book rhs)
        {
            return lhs.CompareTo(rhs) > 0;
        }

        public static bool operator ==(Book lhs, Book rhs)
        {
            return lhs is { } && lhs.Equals(rhs);
        }

        public static bool operator !=(Book lhs, Book rhs)
        {
            return !(lhs == rhs);
        }

        public static bool operator >=(Book lhs, Book rhs)
        {
            return lhs > rhs || lhs == rhs;
        }

        public static bool operator <=(Book lhs, Book rhs)
        {
            return lhs < rhs || lhs == rhs;
        }

        /// <summary>
        /// Gets author of the book.
        /// </summary>
        public string Author { get; }

        /// <summary>
        /// Gets the title of a book.
        /// </summary>
        public string Title { get; }

        /// <summary>
        /// Gets publisher of the book.
        /// </summary>
        public string Publisher { get; }

        /// <summary>
        /// Gets or sets total page count.
        /// </summary>
        public int Pages
        {
            get => this.totalPages;
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(value));
                }

                this.totalPages = value;
            }
        }

        /// <summary>
        /// Gets ISBN code.
        /// </summary>
        public string ISBN { get; }

        /// <summary>
        /// Gets price of the book.
        /// </summary>
        public double Price { get; private set; }

        /// <summary>
        /// Gets ISO Currency symbols.
        /// </summary>
        public string Currency { get; private set; }

        public DateTime PublicationDate
        {
            get
            {
                return this.datePublished;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/> class.
        /// </summary>
        /// <param name="author">Author's name.</param>
        /// <param name="title">Book title.</param>
        /// <param name="publisher">Publisher.</param>
        public Book(string author, string title, string publisher)
        {
            this.Author = author;
            this.Title = title;
            this.Publisher = publisher;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/> class.
        /// </summary>
        /// <param name="author">Author's name.</param>
        /// <param name="title">Book title.</param>
        /// <param name="publisher">Publisher.</param>
        /// <param name="isbn">ISBN code.</param>
        public Book(string author, string title, string publisher, string isbn) :
            this(author, title, publisher)
        {
            this.ISBN = isbn;
        }

        /// <summary>
        /// Marks book as published and assigns publishing date.
        /// </summary>
        /// <param name="date">Publishing date.</param>
        public void Publish(DateTime date)
        {
            this.isPublished = true;
            this.datePublished = date;
        }

        /// <summary>
        /// Gets publication date if book was published. Else returns "NYP".
        /// </summary>
        /// <returns>Publication date.</returns>
        public string GetPublicationDate()
        {
            return this.isPublished ? this.datePublished.Date.ToString(CultureInfo.CurrentCulture) : "NYP";
        }

        /// <summary>
        /// Sets price and ISO currency symbols of the book.
        /// </summary>
        /// <param name="price">Price of the book.</param>
        /// <param name="currency">ISO currency.</param>
        public void SetPrice(double price, RegionInfo currency)
        {
            this.Price = price;
            this.Currency = currency.ISOCurrencySymbol;
        }

        /// <summary>
        /// Determines if two objects of <see cref="Book"/> are equal.
        /// </summary>
        /// <param name="other">Other book.</param>
        /// <returns>Result of comparison.</returns>
        public bool Equals(Book other)
        {
            if (string.IsNullOrEmpty(this.ISBN))
            {
                return other is { } && string.Equals(this.Author, other.Author, StringComparison.OrdinalIgnoreCase)
                                    && string.Equals(this.Title, other.Title, StringComparison.OrdinalIgnoreCase);
            }

            return other is { } && string.Equals(this?.ISBN, other.ISBN);
        }

        /// <summary>
        /// Compares two instances of <see cref="Book"/> class.
        /// </summary>
        /// <param name="other">Other book.</param>
        /// <returns>Result of comparison.</returns>
        public int CompareTo(Book other)
        {
            return string.Compare(this.Title, other.Title, StringComparison.Ordinal);
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.Title} by {this.Author}";
        }

        /// <summary>
        /// Compares object to instance of <see cref="Book"/> class.
        /// </summary>
        /// <param name="obj">Object to compare with.</param>
        /// <returns>Result of comparison.</returns>
        /// <exception cref="InvalidOperationException">Thrown when object is not a <see cref="Book"/>.</exception>
        public int CompareTo(object obj)
        {
            if (!(obj is Book))
            {
                throw new InvalidOperationException("Object is not a Book.");
            }

            return string.Compare(this.Title, ((Book)obj).Title, StringComparison.Ordinal);
        }

        /// <inheritdoc cref="object"/>
        public override int GetHashCode()
        {
            const int hash = 17;
            return hash * 31 + this.ISBN.GetHashCode();
        }

        /// <inheritdoc cref="object"/>
        public override bool Equals(object obj)
        {
            if (!(obj is Book))
            {
                throw new InvalidOperationException("Object is not a Book.");
            }

            return this.Equals((Book)obj);
        }
    }
}